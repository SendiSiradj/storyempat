from django import forms
import datetime
from .models import Project, Post
class Message_Form(forms.ModelForm) :
    name = forms.CharField(label="Nama", required=False, max_length = 27, empty_value ="Anonymous",
    widget=forms.TextInput())
    email = forms.EmailField(required=False,
    widget=forms.EmailInput())
    description = forms.CharField(label="Deskripsi Singkat Proyek",widget=forms.Textarea(), required = True)
    date1= forms.DateTimeField(label= "Tanggal Mulai Proyek",initial= datetime.date.today)
    date2= forms.DateTimeField(label = "Tanggal Selesai Proyek", initial = datetime.date.today)
    category = forms.CharField(label= "Kategori Proyek")

    class Meta:
        model = Post
        fields = ('name','email','description','date1','date2','category')
