from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Project(models.Model) :
    project_title = models.CharField(max_length = 30)
    date = models.DateField()
    description = models.CharField(max_length = 120)
    place = models.CharField(max_length = 150)
    category = models.CharField(max_length = 20)

class Post(models.Model) :
    post = models.CharField(max_length = 120)
    author = models.ForeignKey(User, on_delete = models.CASCADE)
    published_date = models.DateTimeField(default= timezone.now)
