from django.urls import path, include
from  .views import index, pictures, form_result

urlpatterns = [
    path('', index, name = 'index'),
    path('pictures/', pictures, name = 'pictures'),
    path('forms/', form_result, name='form_result')
]
