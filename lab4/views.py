from django.shortcuts import render
from .models import Project, Post
from .forms import Message_Form


# Create your views here.
def index(request) :
    response = {}
    return render(request, 'index.html', response)

def pictures(request) :
    response = {}
    return render(request, 'pictures.html', response)

def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == "POST" and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['description'] = request.POST['description']
        response['date1'] = request.POST['date1']
        response['date2'] = request.POST['date2']
        responese['category'] = request.POST['category']
        post = form.save(commit = False)
        post.user = request.user
        post.save()
        name = form.cleaned_data['name']
        email = form.cleaned_data['email']
        description = form.cleaned_data['description']
        date1 = form.cleaned_data['date1']
        date2 = form.cleaned_data['date2']
        category = form.cleaned_data['category']
        message = Message(name = response['name'], email = response['email'], description= response['description'],
        date1 = response['date1'], date2 = response['date2'], category = response['category'])
        message.save()
        html = 'form_result.html'
        response = {'form' : form, 'name' : name, 'email' : email, 'description' : description, 'date1' : date1, 'date2' : date2, 'category' : category}
        return render(request, html, response)

    else :
        return HttpResponseRedirect('/lab4/')

def form_result(request) :
    form = Message_Form()
    response = {'forms' : form}
    return render(request, 'form_result.html', response)
